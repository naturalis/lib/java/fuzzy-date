module nl.naturalis.fuzzydate {
  requires nl.naturalis.common;
  requires transitive java.xml;
  exports nl.naturalis.fuzzydate;
}