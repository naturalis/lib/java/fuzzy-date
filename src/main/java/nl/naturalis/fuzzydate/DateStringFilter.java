package nl.naturalis.fuzzydate;

import java.time.format.DateTimeFormatter;

/**
 * A {@code DateStringFilter} transforms and/or validates date strings. An instance
 * of it can optionally be added to a {@link ParseAttempt} to "massage" the date
 * strings before they are fed to the {@link DateTimeFormatter}. The {@link
 * #handle(String) handle} method is explicitly allowed to return {@code null}. This
 * causes the {@link FuzzyDateParser} to abort the {@code ParseAttempt} and move on
 * to the next {@code ParseAttempt} (if any). If the {@code handle} method throws a
 * {@link FuzzyDateException}, the date string is treated as definitely not parsable
 * and the {@code ParseAttempt} and all subsequent {@code ParseAttempt}s are aborted
 * for the current date string.
 */
@FunctionalInterface
public interface DateStringFilter {

  /**
   * Validates and/or transforms the date string.
   *
   * @param dateString The date string
   * @return The date string, possibly transformed, or {@code null} to indicate that
   *     the {@code ParseAttempt} should be aborted
   * @throws FuzzyDateException To instruct the parser to give up parsing the
   *     date string
   */
  String handle(String dateString) throws FuzzyDateException;

}
