/**
 * A set of classes aimed at extracting {@code java.time} objects from historical records. See
 * {@link nl.naturalis.fuzzydate.FuzzyDate} for more info.
 */
package nl.naturalis.fuzzydate;
